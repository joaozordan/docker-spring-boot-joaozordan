FROM openjdk:12
ADD target/docker-spring-boot-joaozordan.jar docker-spring-boot-joaozordan.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-joaozordan.jar"]
